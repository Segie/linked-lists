PK
     G�ZX               /Untitled Project 2/PK
     G�ZX��3�  �     /Untitled Project 2/main.c#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key, value;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &value);
                deleteByValue(&head, value);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                scanf("%d", &key);
                printf("Enter value to insert: ");
                scanf("%d", &value);
                insertAfterKey(&head, key, value);
                break;
            case 7:
                printf("Enter value after which to insert: ");
                scanf("%d", &value);
                printf("Enter new value to insert: ");
                scanf("%d", &data);
                insertAfterValue(&head, value, data);
                break;
            case 8:
                printf("Exiting...\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head)
{
    if (head == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    struct Node *temp = head;
    while (temp != NULL)
    {
        printf("%d ", temp->number);
        temp = temp->next;
    }
    printf("\n");
}

void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *temp = *head;
    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
    if (*head == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    struct Node *temp = *head;
    if (temp->number == key)
    {
        *head = temp->next;
        free(temp);
        return;
    }
    while (temp->next != NULL && temp->next->number != key)
    {
        temp = temp->next;
    }
    if (temp->next == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *toBeDeleted = temp->next;
    temp->next = temp->next->next;
    free(toBeDeleted);
}

void deleteByValue(struct Node **head, int value)
{
    if (*head == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    struct Node *temp = *head;
    if (temp->number == value)
    {
        *head = temp->next;
        free(temp);
        return;
    }
    while (temp->next != NULL && temp->next->number != value)
    {
        temp = temp->next;
    }
    if (temp->next == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *toBeDeleted = temp->next;
    temp->next = temp->next->next;
    free(toBeDeleted);
}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != key)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != searchValue)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
}PK 
     G�ZX                            /Untitled Project 2/PK 
     G�ZX��3�  �               2   /Untitled Project 2/main.cPK      �   �    